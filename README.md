# CSGO client text chat mod

#### Installation
1. Move `csgo_saker_chat_mod.txt` inside this folder: `Steam\SteamApps\Common\Counter-Strike Global Offensive\csgo\resource`
2. Add `-language saker_chat_mod` to your launch options

#### Not working?
- `csgo_saker_chat_mod.txt` is placed in the right folder (it needs to be inside the resource folder)
- Launch options is spelled correctly, this is an example that will work: `-novid -language saker_chat_mod`